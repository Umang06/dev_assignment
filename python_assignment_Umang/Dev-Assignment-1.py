# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


'''def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')'''

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
import SparkSql
import json
import requests



class Test:
    def __init__(self):

        return

    #Overwrite the destination table with data from source table
    def overwrite_tables(self, t1, t2):
        try:
            query = f"Truncate {t2}; Insert into {t2} select * from {t1}; Delete {t1}"
            SparkSql.execute_query(query=query)
        except:
            print("ERROR")

    #Append the destination table with data from source table
    def append_tables(self, query=None, t1, t2, columns) :
        try:
            query = f'Insert into {t2} {columns} ; select {columns} from {t1}'
            SparkSql.execute_query(query=query)
        except:
            print("ERROR")

    #Delete the data in the destination table for a given date range ( start_date and end_date of the format YYYY-MM-DD)
    # and then insert data for the same date range from A to B.
    def delete_in_range(self, t1, t2, start_date, end_date):
        try:
            query = f'Delete from {t2} where date between start_date and end_date; Insert into {t2};\
            Select * from {t1} where date between {start_date} and {end_date}'
            self.append_tables({t1},{t2})
        except:
            print("ERROR")


    #Delete the data in the destination table on the basis of a query
    def delete_with_condition(self, t1, t2, given_query):
        try:
            query = f'Delete from {t2} where {given_query}'
            self.append_tables({t1}, {t2})
        except:
            print("ERROR")



if __name__ == '__main__':
      Task = Test()
      Task.overwrite_tables(t1="A", t2="B")
      Task.append_tables(t1="A", t2="B",columns= "id,age")
      Task.delete_in_range(t1="A", t2="B", start_date="2015-05-15", end_date="2025-05-15")
      Task.delete_with_condition(t1="A", t2="B", given_query="age>18")